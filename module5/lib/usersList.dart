import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class UsersList extends StatefulWidget {
  const UsersList({Key? key}) : super(key: key);

  @override
  State<UsersList> createState() => _UsersListState();
}

class _UsersListState extends State<UsersList> {
  final Stream<QuerySnapshot> _myUsers =
      FirebaseFirestore.instance.collection("users").snapshots();

  @override
  Widget build(BuildContext context) {
    TextEditingController _addressFieldController = TextEditingController();
    TextEditingController _emailFieldController = TextEditingController();

    void _delete(doc_Id) {
      FirebaseFirestore.instance
          .collection("users")
          .doc(doc_Id)
          .delete()
          .then((value) => print("Deleted"));
    }

    void _update(data) {
      var collection = FirebaseFirestore.instance.collection("users");
      _addressFieldController.text = data["address"];
      _emailFieldController.text = data["email"];

      showDialog(
          context: context,
          builder: (_) => AlertDialog(
                title: const Text("Update"),
                content: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    TextField(
                      controller: _addressFieldController,
                    ),
                    TextField(
                      controller: _emailFieldController,
                    ),
                    TextButton(
                        onPressed: () {
                          collection.doc(data["doc_id"]).update({
                            "address": _addressFieldController.text,
                            "email": _emailFieldController.text
                          });
                          Navigator.pop(context);
                        },
                        child: const Text("Update"))
                  ],
                ),
              ));
    }

    return StreamBuilder(
      stream: _myUsers,
      builder: (BuildContext context,
          AsyncSnapshot<QuerySnapshot<Object?>> snapshot) {
        if (snapshot.hasError) {
          return const Text("Something went wrong");
        }

        if (snapshot.connectionState == ConnectionState.waiting) {
          return const CircularProgressIndicator();
        }

        if (snapshot.hasData) {
          return Row(
            children: [
              Expanded(
                  child: SizedBox(
                      height: (MediaQuery.of(context).size.height),
                      width: (MediaQuery.of(context).size.width),
                      child: ListView(
                        children: snapshot.data!.docs
                            .map((DocumentSnapshot documentSnapshot) {
                          Map<String, dynamic> data =
                              documentSnapshot.data()! as Map<String, dynamic>;
                          return Column(
                            children: [
                              Card(
                                  child: Column(
                                children: [
                                  ListTile(
                                    title: Text(data['address']),
                                    subtitle: Text(data['email']),
                                  ),
                                  ButtonTheme(
                                      child: ButtonBar(
                                    children: [
                                      OutlinedButton.icon(
                                        onPressed: () {
                                          _update(data);
                                        },
                                        icon: const Icon(Icons.edit),
                                        label: const Text("Edit"),
                                      ),
                                      OutlinedButton.icon(
                                        onPressed: () {
                                          _delete(data["doc_id"]);
                                        },
                                        icon: const Icon(Icons.remove),
                                        label: const Text("Delete"),
                                      )
                                    ],
                                  ))
                                ],
                              ))
                            ],
                          );
                        }).toList(),
                      )))
            ],
          );
        } else {
          return const Text("No data found");
        }
      },
    );
  }
}
