import 'dart:developer';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:module5/usersList.dart';

class Register extends StatefulWidget {
  const Register({Key? key}) : super(key: key);

  @override
  State<Register> createState() => _MyWidgetState();
}

class _MyWidgetState extends State<Register> {
  @override
  Widget build(BuildContext context) {
    TextEditingController nameController = TextEditingController();
    TextEditingController emailController = TextEditingController();
    TextEditingController addressController = TextEditingController();

    Future _register() {
      final name = nameController.text;
      final email = emailController.text;
      final address = addressController.text;

      final ref = FirebaseFirestore.instance.collection("users").doc();

      return ref
          .set({
            "username": name,
            "email": email,
            "address": address,
            "doc_id": ref.id
          })
          .then((value) => log("Users added!"))
          .catchError((onError) => log(onError));
    }

    return Column(
      children: [
        Column(children: [
          Padding(
              padding: const EdgeInsets.symmetric(horizontal: 7, vertical: 7),
              child: TextField(
                controller: nameController,
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular((15)),
                    ),
                    hintText: "Enter your name"),
              )),
          Padding(
              padding: const EdgeInsets.symmetric(horizontal: 7, vertical: 7),
              child: TextField(
                controller: emailController,
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular((15)),
                    ),
                    hintText: "Enter your email"),
              )),
          Padding(
              padding: const EdgeInsets.symmetric(horizontal: 7, vertical: 7),
              child: TextField(
                controller: addressController,
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular((15)),
                    ),
                    hintText: "Enter your Address"),
              )),
          ElevatedButton(
              onPressed: () {
                _register();
              },
              child: const Text("Submit")),
        ]),
        const UsersList()
      ],
    );
  }
}
