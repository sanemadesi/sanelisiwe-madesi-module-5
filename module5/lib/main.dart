import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:module5/register.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
      options: const FirebaseOptions(
          apiKey: "AIzaSyAGFaK628PuBm8aOKld1fgHJ465MooUeNk",
          authDomain: "module-5-webapp.firebaseapp.com",
          projectId: "module-5-webapp",
          storageBucket: "module-5-webapp.appspot.com",
          messagingSenderId: "777186363779",
          appId: "1:777186363779:web:75f26409baf20f176f0eb6"));
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.orange,
      ),
      home: const MyHomePage(title: 'Users List'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: const <Widget>[Register()],
        ),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
